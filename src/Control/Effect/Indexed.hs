
module Control.Effect.Indexed
    ( Effect
    , request
    , runEffect

    , Dispatch
    , (\/)
    , empty

    , Weaves (..)
    , coerce
    , Member
    , Members

    , type (~>)
    )
    where

import Control.Monad (liftM, ap)

import Data.Coerce
import Data.Typeable (Typeable)
import Data.Kind (Constraint)

type f ~> g = ∀ x. f x -> g x

data Nat = Z | S Nat

data SNat (n :: Nat) where
    SZ :: SNat Z
    SS :: SNat n -> SNat (S n)

class Sing n where
  sing :: SNat n

instance Sing Z where
  sing = SZ

instance Sing n => Sing (S n) where
  sing = SS sing

data SingInstance a where
  SingInstance :: Sing a => SingInstance a

singInstance :: SNat n -> SingInstance n
singInstance SZ = SingInstance
singInstance (SS n) =
    case singInstance n of
      SingInstance -> SingInstance

sZ   = SZ
sS n = case singInstance n of
    SingInstance -> SS n

type family At (xs :: [k]) (n :: Nat) :: k where
    (x ': xs) `At` Z   = x
    (x ': xs) `At` S n = xs `At` n

type family Found (xs :: [k]) (x :: k) :: Nat where
    Found (x ': xs) x = Z
    Found (y ': xs) x = S (Found xs x)

class Find xs x where
    index :: SNat (Found xs x)

instance {-# OVERLAPS #-} Find (x ': xs) x where
    index = sZ

instance (Find xs x, Found (y ': xs) x ~ S (Found xs x)) => Find (y ': xs) x where
    index = sS (index @xs @x)

type Member x xs = (Find xs x, x ~ (xs `At` Found xs x), Weaves x)

type family Members xs ys :: Constraint where
    Members '[] ys = ()
    Members (x ': xs) ys = (Member x ys, Members xs ys)

type Action = (* -> *) -> * -> *

data OneOf (effs :: [Action]) m a where
    OneOf :: SNat n -> (effs `At` n) m a -> OneOf effs m a

inject :: ∀ eff effs m. Member eff effs => eff m ~> OneOf effs m
inject = OneOf (index @effs @eff)

newtype Dispatch (effs :: [Action]) m where
    Dispatch :: (∀ n. SNat n -> (effs `At` n) m ~> m) -> Dispatch effs m

dispatch :: Dispatch effs m -> OneOf effs m ~> m
dispatch (Dispatch run) (OneOf n effect) = run n effect

infixr 9 \/

{-# INLINE (\/) #-}
(\/) :: ∀ eff effs m. eff m ~> m -> Dispatch effs m -> Dispatch (eff ': effs) m
(\/) handler (Dispatch rest) = Dispatch $ \case
    SZ   -> handler
    SS m -> rest m

empty :: Dispatch '[] m
empty = Dispatch $ \case {}

infix 1 `runEffect`

newtype Effect (effs :: [Action]) a = Effect
    { runEffect :: ∀ m. Monad m => Dispatch effs m -> m a
    }

class Weaves eff where
    weave :: f ~> g -> eff f ~> eff g

{-# INLINE request #-}
request :: ∀ eff effs a. Member eff effs => eff (Effect effs) ~> Effect effs
request effect = Effect $ \run
    ->  dispatch run
    $   inject
    $   weave @eff (`runEffect` run)
    $   effect

fix :: (a -> a) -> a
fix f = x where x = f x

instance Functor (Effect effs) where
    fmap = liftM

instance Applicative (Effect effs) where
    pure a = Effect $ \_ -> pure a
    (<*>)  = ap

instance Monad (Effect effs) where
    effect >>= callb = Effect $ \run -> do
        a <- runEffect effect run
        callb a `runEffect` run

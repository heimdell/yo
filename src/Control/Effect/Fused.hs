{-# language AllowAmbiguousTypes #-}

-- | Effect system, capable of naturally describing effects with negative self-occurences.
module Control.Effect.Fused
    ( -- * Effects
      Effect
    , request
    , runEffects

      -- * Effect membership
    , Member
    , Members

      -- * DSL for an interpreter
    , Dispatch
    , (/\)
    , empty
    , remove

    , Weaves(..)

      -- * Natural transformation
    , type (~>)
    )
    where

import Data.Array (listArray, (!), Array, (//), bounds, elems)
import Data.Kind (Type, Constraint)
import Data.Proxy (Proxy (Proxy))

import GHC.Types (Any)
import GHC.TypeLits (Nat, type (+), KnownNat, natVal)

import Unsafe.Coerce (unsafeCoerce)

import Control.Monad (ap, liftM)

import Debug.Trace

-- | For "containers" @f@ and @g@, @f ~> g@ is a function, that for any type @x@
--   will transform @f x@ into @g x@.
type f ~> g = ∀ x. f x -> g x

-- | First argument is a resulting `Effect effs` monad to be applied on
--   elimination site.
type Action = (Type -> Type) -> Type -> Type

-- | Stores one of @effs@ applied to @a@ as `Any`.
--   The parameters are given to `Any` to protect from `Data.Coerce.coerce`.
--
--   `Int` index is a position of the effect from /right/ side of the list.
--   This is done, so @OneOf xs@ always have the same memory representation as
--   @OneOf (ys ++ xs)@, anc can be `unsafeCoerced` into it w/o giving segfault.
data OneOf (effs :: [Action]) (a :: Type)
    = Case Int (Any effs a)

-- | For all @effs@, stores @eff ~> m@ as `Any`.
--
--   `Int` index is a position of the effect from /right/ side of the list.
newtype Dispatch (effs :: [Action]) (m :: Type -> Type)
    = Dispatch (Array Int (Any effs m))

-- | Find position of effect.
type family Position (eff :: Action) (effs :: [Action]) :: Nat where
    Position x (x ': xs) = 0
    Position x (y ': xs) = 1 + Position x xs

-- | Find position of effect.
type family Length (effs :: [Action]) :: Nat where
    Length '[]       = 0
    Length (_ ': xs) = 1 + Length xs

-- | This is not actual membership constraint (`Position` will report
--   @eff@ not being in @effs@ by itself), but it provides the same
--   functionality as @Member@ constraint of naive implementation.
type Member eff effs = (KnownNat (Position eff effs), EffectList effs)
type EffectList effs = KnownNat (Length effs)

-- | Checks if each of @xs@ is in @ys@.
type family Members xs ys :: Constraint where
    Members '[]        ys = ()
    Members  (x ': xs) ys = (Member x ys, Members xs ys)

-- | Get the index of @eff@ in the @effs@ list.
index :: ∀ eff effs. Member eff effs => Int
index = len @effs - 1 - fromIntegral (natVal (Proxy :: Proxy (Position eff effs)))

-- | Get the length of @effs@ list.
len :: ∀ effs. EffectList effs => Int
len = fromIntegral $ natVal (Proxy :: Proxy (Length effs))

-- | Lift an @eff@ into `OneOf`.
inject :: ∀ eff effs a. Member eff effs => eff (Effect effs) ~> OneOf effs
inject fa = Case (index @eff @effs) (unsafeCoerce fa)

-- | Push an effect dispatcher in the stack of dispatchers.
--
--   Effect dispatcher also contains an argument to dispatch /all/ other effects.
--
--   We are pushing the new dispatcher in the end of the list, so
--   @Dispatch (ys ++ xs)@ can be applied via `unsafeCoerce` to @OneOf xs@.
infixr 1 /\
(/\)
    ::  (   Dispatch effsN m
        ->  eff (Effect effsN)
        ~> m
        )
    -> Dispatch effs m
    -> Dispatch (eff ': effs) m
(/\) one (Dispatch rest) =
    Dispatch
        $ listArray (l, h + 1)
        $ map unsafeCoerce (elems rest) ++ [unsafeCoerce one]
  where
    (l, h) = bounds rest

-- | Dispatcher of nothing.
empty :: Dispatch '[] m
empty = Dispatch $ listArray (0, -1) []

-- | Remove effect from the list by evaluating it through remaining effects.
remove
    ::  forall eff effs
    .   IdentityDispatch effs
    =>  (   Dispatch (eff ': effs) (Effect effs)
        ->  eff (Effect (eff ': effs))
        ~>  Effect effs
        )
    ->  Effect (eff ': effs)
    ~>  Effect         effs
remove handle effect = effect `runEffects` (handle /\ identity)

-- | Run the dispatcher against the `OneOf`.
dispatch :: Dispatch effs m -> (OneOf effs ~> m)
dispatch (Dispatch elims) (Case index body) = ((elims ! index) `unsafeCoerce` elims) body

-- | Effect container.
newtype Effect (effs :: [Action]) (a :: Type) = Effect
    { -- | Given effect dispatchers, run an effectful computation.
      runEffects :: ∀ m. Monad m => Dispatch effs m -> m a
    }

-- | Lift an effect into an action.
request :: ∀ eff effs. Member eff effs => eff (Effect effs) ~> Effect effs
request u = Effect (`dispatch` inject u)

instance Monad (Effect effs) where
    Effect runner >>= callb = Effect $ \impl -> do
        a <- runner impl
        callb a `runEffects` impl

instance Applicative (Effect effs) where
    pure a = Effect $ \_ -> pure a
    (<*>)  = ap

instance Functor (Effect effs) where
    fmap = liftM

-- | Ability to transform `Effect effs` into itself.
class IdentityDispatch effs where
    identity :: Dispatch effs (Effect effs)

instance IdentityDispatch '[] where
    identity = empty

instance (Weaves eff, IdentityDispatch effs, EffectList (eff ': effs)) => IdentityDispatch (eff ': effs) where
    identity = rewire /\ unsafeCoerce (identity :: Dispatch effs (Effect effs))

-- | Ability to handle recursion points of global effect.
class Weaves eff where
    weave :: (f ~> g) -> (eff f ~> eff g)

-- | Transform recusrion points.
rewire :: (Weaves eff, Member eff effs) => Dispatch effsN (Effect effs) -> eff (Effect effsN) ~> Effect effs
rewire dispatchers eff = request $ weave (`runEffects` dispatchers) eff

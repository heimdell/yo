
{-|
    Example usage of effects.
-}
-- module Control.Effect.Example where

import Control.Effect.Indexed
import Control.Exception (ArithException(..), ArrayException(..))
import Control.Monad
import Control.Monad.Catch
import Control.Monad.Reader hiding (lift)
import Control.Monad.IO.Class

import Data.Array
import Data.IORef
import Data.Kind (Type)
import Data.String.Interpolate (i)

import Debug.Trace

import Prelude hiding (log)

import System.Environment

-- | Errors/recovery component.

data Throws m (a :: Type) where
    Raise  :: Exception e => e -> Throws m a
    Rescue :: Exception e => m a -> (e -> m a) -> Throws m a

-- | The "throw".
raise :: (Member Throws effs, Exception e) => e -> Effect effs a
raise e = request (Raise e)

-- | The "catch".
rescue :: (Member Throws effs, Exception e) => Effect effs a -> (e -> Effect effs a) -> Effect effs a
rescue block rescuer = request (Rescue block rescuer)

-- | Exception throwing/catching is delegated to the result monad.
throwing :: MonadCatch m => (Throws m ~> m)
throwing (Raise e)              = throwM e
throwing (Rescue block rescuer) = catch block rescuer

instance Weaves Throws where
    weave nat (Raise e) = Raise e
    weave nat (Rescue block handler) = Rescue (nat block) (nat . handler)

data Env e m (a :: Type) where
    Env      :: Env e m e
    Override :: (e -> e) -> m a -> Env e m a

env :: ∀ e effs. Member (Env e) effs => Effect effs e
env = request (Env @e)

override :: ∀ e effs. Member (Env e) effs => (e -> e) -> Effect effs ~> Effect effs
override delta action = request (Override delta action)

asking :: MonadReader e m => (Env e m ~> m)
asking  Env                    = ask
asking (Override delta action) = local delta action

instance Weaves (Env e) where
    weave nat  Env                   = Env
    weave nat (Override delta block) = Override delta (nat block)

data Teletype m a where
    WriteLine :: String -> Teletype m ()
    ReadLine  :: Teletype m String

writeLine :: Member Teletype effs => String -> Effect effs ()
writeLine s = request (WriteLine s)

readLine :: Member Teletype effs => Effect effs String
readLine = request ReadLine

teletyping :: MonadIO m => (Teletype m ~> m)
teletyping (WriteLine s) = liftIO $ putStrLn s
teletyping  ReadLine     = liftIO $ getLine

instance Weaves Teletype where
    weave _ = coerce

-- Lifting

-- | So you can launch missiles!
data Lifts n m a where
    Lift :: n a -> Lifts n m a

lift :: Member (Lifts n) effs => n a -> Effect effs a
lift na = request (Lift na)

liftingIO :: MonadIO m => Lifts IO m ~> m
liftingIO (Lift action) = liftIO action

instance Weaves (Lifts m) where
    weave _ = coerce

-- | Entry point.
main :: IO ()
main = do
    flip runReaderT "shell" $ do
        override ("root-" ++) $ do
            override ("loot-" ++)  $ do
                s <- env
                lift $ putStrLn "hello from IO!"
                raise (IndexOutOfBounds s)
          `rescue` \(e :: ArrayException) -> do
            writeLine [i|an exception #{e} occured|]
      `runEffect`
        (   throwing
        \/  teletyping
        \/  liftingIO
        \/  asking
        \/  empty
        )
